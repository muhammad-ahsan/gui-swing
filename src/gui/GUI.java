/*
 * Programmed by Muhammad Ahsan
 * muhammad.ahsan@gmail.com
 * Erasmus Mundus Master in Data Mining and Knowledge Management
 * France - Italy
 */
package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Ahsan
 */
public class GUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        NewJFrame frame1 = new NewJFrame();
        frame1.setTitle("Java Default Look and Feel");
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel label = new JLabel("Java Default Look and Feel");
        frame1.add(label);
        frame1.pack();
        frame1.setVisible(true);


        try {
            // Set System specific Java L&F
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
            NewJFrame frame = new NewJFrame();
            frame.setTitle("System's Look and Feel");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JLabel label1 = new JLabel("System's Look and Feel");
            frame.add(label1);
            frame.pack();
            frame.setVisible(true);
        } catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            // handle exception
        }
    }
}
